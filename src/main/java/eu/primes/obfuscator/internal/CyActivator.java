package eu.primes.obfuscator.internal;

import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.TaskFactory;
import org.osgi.framework.BundleContext;

/**
 * Obfuscator is a simple Cytoscape app that obfuscate names of nodes in networks by replacing them with
 * random names. When more than one networks are selected, nodes with the same names in different networks
 * will be given identical new names so as to maintain correspondence among nodes in different networks.
 * There's also the option to obfuscate network names. The goal is for users to be able to export networks 
 * (with or without graphics)  without any trace of the original node names remaining in the resulting file. 
 * 
 * @author Ivan Hendy Goenawan
 */
public class CyActivator extends AbstractCyActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		CyApplicationManager appMgr = getService(context, CyApplicationManager.class);
		CyNetworkViewManager netViewManager = getService(context, CyNetworkViewManager.class);
		CyNetworkTableManager netTableManager = getService(context, CyNetworkTableManager.class);
		ObfuscateTaskFactory taskFactory = new ObfuscateTaskFactory(appMgr, netViewManager, netTableManager);
		
		Properties properties = new Properties();
		properties.put("title", "Obfuscate Selected Networks");
		properties.put("preferredMenu", "Apps");
		
		registerService(context, taskFactory, TaskFactory.class, properties);
	}

}
